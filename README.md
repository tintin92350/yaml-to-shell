# Yaml to shell

## Description

Yaml to shell is a tiny tool using python (3.8+) to generate an export shell command of a yaml file.

## How it works ?

First let take a simple yaml file that we want to extract environment variables (for example, we want to use them as
local for development purpose)

Create a file.yaml :

```yaml
application:
  project:
    name: project-name
    version: 1.0
  database:
    URL: https://example.org/database
    DATABASE_USER: user
```

When the file is created just run the following command:

```shell
$ yml2shell ~/file.yaml
```

It will output the following command:

```shell
Parsing file.yml document

export name=project-name version=1.0 URL=https://example.org/database DATABASE_USER=user
```

## Using bash

The provided script `yml2shell.bash` allow you to export directly the environments variables into the current shell
session.

Exemple:

```shell
$ . ./yml2shell.bash ~/file.yaml
```

or

```shell
$ source ./yml2shell.bash ~/file.yaml
```

you can't use `export` function because it will only make accessible the variables into the child process (the script
himself).

You will be able to access to all environment variables :

```shell
$ echo $URL
https://example.org/database
```

## How to install

Follow the instructions:

```shell
$ git clone git@gitlab.com:tintin92350/yaml-to-shell.git
$ cd yaml-to-shell
$ pip install -U pyinstaller
$ pip install -r requirements.txt
$ pyinstaller --onefile src/yml2shell.py
$ cp dist/yml2shell /usr/bin
```