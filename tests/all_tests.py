import unittest

import yml2shell


class IsPrimitiveTest(unittest.TestCase):
    def test_if_primitive_are_recognized_as_primitive(self):
        self.assertEqual(True, main.is_primitive("string"))
        self.assertEqual(True, main.is_primitive(5))
        self.assertEqual(True, main.is_primitive(4.5))
        self.assertEqual(True, main.is_primitive(True))
        self.assertEqual(True, main.is_primitive(["array"]))

    def test_if_none_primitive_are_not_recognized_as_primitive(self):
        complex_object: {} = {'name': 'test'}

        self.assertEqual(False, main.is_primitive(complex_object))


class ExportVariableInShellTest(unittest.TestCase):

    def test_if_simple_dict_is_exported_correctly(self):
        complex_object: {} = {'name': 'test'}
        exported: str = "export name=test"
        self.assertEqual(exported, main.export_variables_in_shell(complex_object))

    def test_if_multiple_value_in_dict_is_exported_correctly(self):
        complex_object: {} = {'name': 'test', 'version': 1.0, 'git': True, 'members': ['john', 'doe', 'right']}
        exported: str = "export name=test version=1.0 git=true members=\"['john', 'doe', 'right']\""
        self.assertEqual(exported, main.export_variables_in_shell(complex_object))


if __name__ == '__main__':
    unittest.main()
