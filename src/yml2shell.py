import getopt
import os.path
import sys

import yaml

primitive = (int, float, bool, str, list)


def is_primitive(object_to_test: any):
    """
    Returns true if object instance is amongst defined primitive one
    :param object_to_test: Object to test if primitive or not
    :return: boolean
    """
    return isinstance(object_to_test, primitive)


def get_all_variables(yaml_content: any, output_map=None) -> dict:
    """
    This is a recursive function that browse the content to search for primitive yaml variables.
    If we have a primitive value so store it to the output map.
    But if we have a more complex structure (i.e. Object) so we go through the object content,
    and do the same thing again.

    :param yaml_content: YAML Content read by the yaml loader
    :param output_map: The map produced by the function to map key and value of variables we want to extract from yaml
    :return: Nothing
    """

    if output_map is None:
        output_map = dict()

    for variable_key, variable_value in yaml_content.items():
        if is_primitive(variable_value):
            output_map[variable_key] = variable_value
        else:
            get_all_variables(variable_value, output_map)

    return output_map


def export_variables_in_shell(variables: dict, in_bash_mode: bool) -> str:
    """
    Export the variables inside dictionary into a usable shell command
    :param variables: key-value pair of variables to export
    :param in_bash_mode: should we export for a script
    :return: Exported shell command as string
    """
    cmd_output = ""

    if not in_bash_mode:
        cmd_output = "export"

    for variable_key, variable_value in variables.items():

        # Lowercase only boolean values, keep other same case
        if isinstance(variable_value, bool):
            variable_value = variable_value.__str__().lower()
        # If we have an array we add some quotes around the array value
        elif isinstance(variable_value, list):
            variable_value = "\"" + variable_value.__str__() + "\""

        cmd_output += f" {variable_key}={variable_value}"

    return cmd_output


if __name__ == "__main__":

    # Get all argument and options
    opts, args = getopt.getopt(sys.argv[2:], "hb", ["bash"])

    # Check if user entered the yaml file location
    if len(sys.argv[1:]) - len(opts) < 1:
        print("[ERROR] /!\\ You have no submitted any yaml file where extract variables !")
        sys.exit(1)

    yaml_file_path = sys.argv[1]

    # Special mode for bash script use (output is only wanted data)
    bash_mode: bool = False

    # Read options
    for opt, arg in opts:
        if opt in ["-b", "--bash"]:
            bash_mode = True

    # Check if yaml file exists
    if not os.path.exists(yaml_file_path):
        print(f"[ERROR] /!\\ The yaml file {yaml_file_path} doesn't exists")
        sys.exit(1)

    if not bash_mode:
        print(f"Parsing {yaml_file_path} document")

    with open(yaml_file_path) as file:
        content = yaml.load(file, Loader=yaml.FullLoader)
        output: dict = get_all_variables(content)
        shell_command = export_variables_in_shell(output, bash_mode)

        if not bash_mode:
            print("\n")

        print(shell_command, flush=True)

        if not bash_mode:
            print("\n")
