#!/usr/bin/env bash

#
# Name: yml2shell.bash
# Description: Use this script to automatically wrap python yml2shell script to the environment instead of manually
# copying the export function into shell
#

yaml_file=$1

exported=$(python3 src/yml2shell.py "$yaml_file" --bash)
errorPattern='[ERROR]'
if [[ $exported == *"$errorPattern"* ]] ; then
  echo "An error occured: $exported"
else
  export exported
  echo "Yaml file successfully exported and injected into current environment"
fi
